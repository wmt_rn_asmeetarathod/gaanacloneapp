import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Routes from 'src/router/Routes';

// Screens Name

import Login from 'src/screens/UnAuthenticated/LoginScreen';
import SignUp from 'src/screens/UnAuthenticated/SignUpScreen';

const Stack = createStackNavigator();

const navigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={Routes.Login}
            screenOptions={({navigation}) => ({
                headerTitleAlign: 'center',
            })}>
            <Stack.Screen
                name={Routes.Login}
                component={Login}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name={Routes.SignUp}
                component={SignUp}
                options={{headerShown: false}}
            />
        </Stack.Navigator>
    );
};

export default navigator;
