const Routes = {
    /*  Root Stacks    */

    Authenticated: 'Authenticated',
    UnAuthenticated: 'UnAuthenticated',
    Splash: 'Splash',

    /*  Non-Authenticated Routes    */
    Login: 'Login',
    SignUp: 'SignUp',

    /*  Authenticated Routes    */

    Home: 'Home',
    Category: 'Category',
    AllSongs: 'AllSongs',
    NewSongs: 'NewSongs',
    RetroHits: 'RetroHits',
    TopHits: 'TopHits',
    Play: 'Play',
};
export default Routes;
