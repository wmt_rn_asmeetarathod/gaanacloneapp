import React from 'react';
import {View, StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Routes from 'src/router/Routes';
import {ThemeUtils, Color, Strings} from 'src/utils';
// Screens Name

import Home from 'src/screens/Authenticated/HomeScreen';
import Category from 'src/screens/Authenticated/CategoryScreen';
import NewSongs from 'src/screens/Authenticated/NewSongsScreen';
import RetroHits from 'src/screens/Authenticated/RetroHitsScreen';
import TopHits from 'src/screens/Authenticated/TopHitsScreen';
import AllSongs from 'src/screens/Authenticated/AllSongsScreen';
import Play from '../screens/Authenticated/PlayScreen';

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

export const TabNavigator = (props) => {
    return (
        <Tab.Navigator
            initialRouteName={Routes.AllSongs}
            tabBarOptions={{
                activeTintColor: Color.PINK01,
                inactiveTintColor: Color.DARK_LIGHT_BLACK,

                labelStyle: {
                    fontSize: ThemeUtils.FontSize.fontSmall,
                    fontFamily: ThemeUtils.FontStyle.regular,
                    textTransform: 'none',
                },
                indicatorStyle: {
                    borderWidth: 1,
                    borderColor: Color.PINK01,
                },
                style: {
                    backgroundColor: Color.WHITE,
                },
            }}>
            <Tab.Screen
                component={AllSongs}
                name={Routes.AllSongs}
                options={{title: Strings.allSongs}}
            />
            <Tab.Screen
                component={NewSongs}
                name={Routes.NewSongs}
                options={{title: Strings.newSongs}}
            />
            <Tab.Screen
                component={RetroHits}
                name={Routes.RetroHits}
                options={{title: Strings.retroHits}}
            />
            <Tab.Screen
                component={TopHits}
                name={Routes.TopHits}
                options={{title: Strings.topHits}}
            />
        </Tab.Navigator>
    );
};

const navigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={__DEV__ ? Routes.Home : Routes.Home}
            screenOptions={({navigation}) => ({
                headerTitleAlign: 'center',
                headerTitleStyle: {
                    fontFamily: ThemeUtils.FontStyle.regular,
                },
            })}>
            <Stack.Screen
                name={Routes.Home}
                component={Home}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name={Routes.Category}
                component={Category}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name={Routes.Play}
                component={Play}
                // options={{headerShown: false}}
            />
        </Stack.Navigator>
    );
};

export default navigator;
