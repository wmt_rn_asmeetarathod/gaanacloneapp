export const Color = {
    /*Main Theme Colors*/
    PRIMARY: '#D223A8', //#AD46BE
    PRIMARY_DARK: '#6B1EA3',
    ACCENT_COLOR: '#00bcd4',
    /*Text Colors*/
    TEXT_PRIMARY: '#000000',
    TEXT_SECONDARY: '#696969',
    TEXT_PLACEHOLDER: '#a8a8a8',
    TEXT_COLOR_ACTIVE: '#00bcd4',
    /*Other Colors*/
    PRIMARY_BACKGROUND: '#EEEEEE',
    DIVIDER_COLOR: 'rgba(255,255,255,0.2)',
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    DARK_LIGHT_BLACK: 'rgba(0, 0, 0, 0.5)',
    TRANSPARENT: 'transparent',
    PINK01: '#D223A8',
    TRANSPARENT_PINK: '#FFDDEE',
};
