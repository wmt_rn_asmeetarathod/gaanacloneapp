import React from 'react';

import {View, Image} from 'react-native';
import {styles} from './styles';

import {Label} from 'src/component';
import {Color, Strings} from 'src/utils';
import APP_LOGO from 'src/assets/images/app_logo.png';

const AppTextLogo = () => {
    return (
        <View style={styles.vwLogoStyle}>
            <Image style={styles.logoStyle} source={APP_LOGO} />
            <Label xlarge color={Color.WHITE}>
                {Strings.gaanaCloneCaps}
            </Label>
        </View>
    );
};

export default AppTextLogo;
