import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export default StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    categoryImageStyle: {
        height: ThemeUtils.relativeWidth(40),
        width: ThemeUtils.relativeWidth(40),

        // flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // opacity: 0.6,
    },
    radius: {
        borderRadius: ThemeUtils.relativeWidth(20),
    },
    itemStyle: {
        height: ThemeUtils.relativeWidth(40),
        width: ThemeUtils.relativeWidth(40),
        borderRadius: ThemeUtils.relativeWidth(20),
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginBottom: 20,
        // backgroundColor: Color.PINK01,
    },
    titleStyle: {
        backgroundColor: Color.CELLO,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        padding: 15,
        textAlign: 'center',
        borderRadius: ThemeUtils.relativeWidth(20),
    },
    lblStyle: {
        marginVertical: 20,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        paddingHorizontal: ThemeUtils.relativeRealWidth(4),
        paddingVertical: ThemeUtils.relativeRealHeight(2),
    },
});
