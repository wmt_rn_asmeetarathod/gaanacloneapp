import React, {useState} from 'react';

import {
    ImageBackground,
    View,
    Image,
    ScrollView,
    ToastAndroid,
    FlatList,
} from 'react-native';
import {AppButton, AppInput, Label, Ripple} from 'src/component';
import styles from './styles';
import {ThemeUtils, Color, Strings} from 'src/utils';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpg';
import APP_LOGO from 'src/assets/images/app_logo.png';
import {CommonActions} from '@react-navigation/native';
import Routes from 'src/router/Routes';
import CATEGORY_BACKGROUND from 'src/assets/images/blue_background.jpg';
import SELECTED_CATEGORY from 'src/assets/images/selected_category.jpg';

const Category = (props) => {
    const data = [
        'Retro',
        'Bollywood',
        'Soul',
        'Dance',
        'Blues',
        'Jazz',
        'Country',
        'Hip Hop',
    ];

    /*  Life-cycles Methods */

    const goToHomeScreen = () => {
        props.navigation.navigate(Routes.Home);
    };

    /*  Custom-Component sub-render Methods */
    const RenderItemCompo = ({itemData}) => {
        const [pressed, setPressed] = useState(false);

        return (
            <Ripple
                style={styles.itemStyle}
                rippleContainerBorderRadius={ThemeUtils.relativeWidth(20)}
                onPress={() => setPressed(!pressed)}>
                <ImageBackground
                    source={pressed ? SELECTED_CATEGORY : CATEGORY_BACKGROUND}
                    style={[
                        styles.categoryImageStyle,
                        // eslint-disable-next-line react-native/no-inline-styles
                        {opacity: pressed ? 0.7 : 0.9},
                    ]}
                    imageStyle={styles.radius}>
                    <Label xlarge style={styles.titleStyle} color={Color.WHITE}>
                        {itemData}
                    </Label>
                </ImageBackground>
            </Ripple>
        );
    };

    return (
        <ImageBackground style={styles.backgroundImage} source={APP_BACKGROUND}>
            <View style={styles.mainContainer}>
                <Label
                    style={styles.lblStyle}
                    mb={20}
                    mt={20}
                    color={Color.WHITE}>
                    {'Choose your Category'}
                </Label>
                <FlatList
                    style={styles.flatListStyle}
                    data={data}
                    renderItem={({item}) => <RenderItemCompo itemData={item} />}
                    keyExtractor={(item) => item}
                    showsVerticalScrollIndicator={false}
                    numColumns={2}
                />
                <AppButton
                    click={goToHomeScreen}
                    width={ThemeUtils.relativeRealWidth(82)}
                    mt={ThemeUtils.relativeRealHeight(2)}
                    mb={ThemeUtils.relativeRealHeight(2)}
                    backgroundColor={Color.PINK01}
                    textColor={Color.WHITE}
                    style={styles.btnStyle}>
                    {Strings.go}
                </AppButton>
            </View>
        </ImageBackground>
    );
};

export default Category;
