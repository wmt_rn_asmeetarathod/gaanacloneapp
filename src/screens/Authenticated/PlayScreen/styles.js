import {StyleSheet} from 'react-native';
import {ThemeUtils, Color} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        backgroundColor: Color.WHITE,
        marginTop: ThemeUtils.relativeHeight(20),
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        padding: 30,
    },
    logoStyle: {
        height: ThemeUtils.relativeWidth(60),
        width: ThemeUtils.relativeWidth(60),
        borderRadius: ThemeUtils.relativeWidth(10),
        backgroundColor: Color.WHITE,
    },
    logoVw: {
        justifyContent: 'center',
        alignItems: 'center',
        top: -ThemeUtils.relativeHeight(10),
    },
    backgroundImage: {
        flexGrow: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        // paddingHorizontal: ThemeUtils.relativeRealWidth(4),
        // paddingVertical: ThemeUtils.relativeRealHeight(2),
    },
    itemStyle: {
        height: ThemeUtils.relativeWidth(65),
        // width: ThemeUtils.relativeWidth(30),
        margin: ThemeUtils.relativeWidth(2),
        // backgroundColor: Color.PINK01,
        // padding: ThemeUtils.relativeWidth(2),
        justifyContent: 'space-between',
        // alignItems: 'center',
    },
    trackImageStyle: {
        alignSelf: 'flex-start',
        height: ThemeUtils.relativeWidth(45),
        // width: ThemeUtils.relativeWidth(25),
        borderRadius: ThemeUtils.relativeWidth(5),
    },
    vwTrendingSongs: {
        marginBottom: ThemeUtils.relativeHeight(2),
    },
    vwEvergreenHits: {},
    vwNameStyle: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 5,
    },
    flatListStyle: {
        backgroundColor: Color.TRANSPARENT,
        paddingHorizontal: 20,
    },
    vwPlayIcons: {
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
    },
    iconStyle: {
        padding: 20,
    },
    playIconStyle: {
        padding: 15,
        borderRadius: 50,
        backgroundColor: Color.PRIMARY_DARK,
        opacity: 0.9,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
