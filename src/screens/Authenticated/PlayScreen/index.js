import React, {useEffect, useState} from 'react';

import {
    View,
    FlatList,
    ImageBackground,
    Image,
    Dimensions,
    ScrollView,
} from 'react-native';
import {APIRequest} from 'src/api';
import {styles} from './styles';
import SoundPlayer from 'react-native-sound-player';
import {Label, Ripple} from 'src/component';
import {CommonStyle, Color, ThemeUtils, Strings} from 'src/utils';
import {AppTextLogo} from 'src/component';
import {TabNavigator} from 'src/router/Authenticated';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';

// images
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpg';
import PLAY_IMAGE from 'src/assets/images/PLAY_IMAGE.jpg';
import APP_LOGO from 'src/assets/images/app_logo.png';

const Play = (props) => {
    const {artistName, songName, imageUri} = props.route.params;
    const [playing, setPlaying] = useState(false);

    /*  Life-cycles Methods */
    /*  Public Interface Methods */

    /*  Validation Methods  */

    /*  UI Events Methods   */
    const onBackPress = () => {};

    const onForwardPress = () => {};

    const onPlayPausePress = async () => {
        setPlaying(!playing);
        SoundPlayer.loadUrl(
            'http://listen.vo.llnwd.net/g3/prvw/8/9/6/6/1/2305616698.mp3',
        );

        const info = await SoundPlayer.getInfo();
        console.log('info ==>> ', info);

        await SoundPlayer.getInfo()
            .then(({duration, currentTime}) => {
                console.log(
                    'duration => ',
                    duration,
                    'currentTime => ',
                    currentTime,
                );

                if (playing) {
                    if (currentTime === duration) {
                        SoundPlayer.stop();
                        console.log('STOP');
                    }
                    if (currentTime < duration) {
                        SoundPlayer.resume();
                        console.log('RESUME');
                    }
                } else {
                    if (currentTime === 0) {
                        SoundPlayer.play();
                        console.log('PLAY');
                    }
                    if (currentTime > 0) {
                        SoundPlayer.pause();
                        console.log('PAUSE');
                    }
                }
            })
            .catch((error) => {
                console.log('SoundPlayer error ==>> ', error);
            });
    };

    return (
        <ImageBackground style={styles.backgroundImage} source={APP_BACKGROUND}>
            <View style={styles.container}>
                <View style={styles.logoVw}>
                    <Image style={styles.logoStyle} source={PLAY_IMAGE} />
                    <Label xlarge mt={10}>
                        {songName}
                    </Label>
                    <Label>{artistName}</Label>
                </View>
                <View style={styles.vwPlayIcons}>
                    <Ripple
                        rippleContainerBorderRadius={50}
                        onPress={() => onBackPress()}
                        style={styles.iconStyle}>
                        <IonIcon
                            name="play-skip-back"
                            style={styles.searchIcon}
                            color={Color.BLACK}
                            size={30}
                        />
                    </Ripple>
                    <Ripple
                        rippleContainerBorderRadius={50}
                        onPress={() => onPlayPausePress()}
                        style={styles.playIconStyle}>
                        <IonIcon
                            name={playing ? 'pause-outline' : 'play'}
                            // style={styles.playIconStyle}
                            color={Color.WHITE}
                            size={30}
                        />
                    </Ripple>
                    <Ripple
                        rippleContainerBorderRadius={50}
                        onPress={() => onForwardPress()}
                        style={styles.iconStyle}>
                        <IonIcon
                            name="play-skip-forward"
                            style={styles.searchIcon}
                            color={Color.BLACK}
                            size={30}
                        />
                    </Ripple>
                </View>
            </View>
        </ImageBackground>
    );
};

export default Play;
