import React, {useEffect, useState} from 'react';

import {
    View,
    FlatList,
    ImageBackground,
    Image,
    Dimensions,
    ScrollView,
} from 'react-native';
import {APIRequest} from 'src/api';
import {styles} from './styles';

import {Label, Ripple} from 'src/component';
import {CommonStyle, Color, ThemeUtils, Strings} from 'src/utils';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpg';
import APP_LOGO from 'src/assets/images/app_logo.png';
import {AppTextLogo} from 'src/component';
import {TabNavigator} from '../../../router/Authenticated';

const Home = (props) => {
    /*  Life-cycles Methods */
    /*  Public Interface Methods */

    /*  Validation Methods  */

    /*  UI Events Methods   */

    const onPressIcon = () => {};

    return (
        <ImageBackground style={styles.backgroundImage} source={APP_BACKGROUND}>
            <AppTextLogo />
            <View style={styles.bottomContainer}>
                <TabNavigator />
            </View>
        </ImageBackground>
    );
};

export default Home;
