import React, {useEffect, useState} from 'react';

import {
    View,
    FlatList,
    ImageBackground,
    Image,
    Dimensions,
    ScrollView,
} from 'react-native';
import {APIRequest} from 'src/api';
import {styles} from './styles';

import {Label, Ripple} from 'src/component';
import {CommonStyle, Color, ThemeUtils, Strings} from 'src/utils';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpg';
import APP_LOGO from 'src/assets/images/app_logo.png';

const NewSongs = (props) => {
    const [data, setData] = useState([]);
    const width = Dimensions.get('window').width;
    const [userTopTrackData, setUserTopTrackData] = useState([]);
    const [topAlbumData, setTopAlbumData] = useState([]);

    /*  Life-cycles Methods */
    useEffect(() => {
        // Tag Top Tracks
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(
                '/2.0/?method=tag.gettoptracks&tag=disco&api_key=c6dddbc6cda161751a9266b641f0c1b9&format=json',
            )
            .response(onResponse)
            .error(onError)
            .build()
            .doRequest();

        // Top Albums
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(
                '/2.0/?method=artist.gettopalbums&artist=cher&api_key=c6dddbc6cda161751a9266b641f0c1b9&format=json',
            )
            .response(onTopAlbumResponse)
            .error(onTopAlbumError)
            .build()
            .doRequest();

        // User Top Tracks
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(
                '/2.0/?method=user.gettoptracks&user=rj&api_key=https://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=cher&api_key=c6dddbc6cda161751a9266b641f0c1b9&format=json&format=json',
            )
            .response(onUserTopTrackResponse)
            .error(onUserTopTrackError)
            .build()
            .doRequest();
    }, []);

    /*  Public Interface Methods */

    /*  Validation Methods  */

    /*  UI Events Methods   */

    // Get Tag Top Tracks
    const onResponse = (res) => {
        console.log('onResponse res ==>> ', res);
        setData(res.data.tracks.track);
    };

    const onError = (e) => {
        console.log('onError e ==>> ', e);
    };

    // Get Top Albums
    const onTopAlbumResponse = (res) => {
        console.log('onTopAlbumResponse res ==>> ', res);
        setTopAlbumData(res.data.topalbums.album);
    };

    const onTopAlbumError = (e) => {
        console.log('onTopAlbumError e ==>> ', e);
    };

    // Get User Top Tracks
    const onUserTopTrackResponse = (res) => {
        console.log('onUserTopTrackResponse res ==>> ', res);
        setUserTopTrackData(res.data.toptracks.track);
    };

    const onUserTopTrackError = (e) => {
        console.log('onUserTopTrackResponse e ==>> ', e);
    };

    const onPressIcon = () => {};

    /*  Custom-Component sub-render Methods */
    const RenderItemCompo = ({itemData}) => {
        const {name, artist, image} = itemData;

        return (
            <Ripple
                style={[styles.itemStyle, {width: width / 2 - 30}]}
                rippleContainerBorderRadius={20}
                // onPress={() => goToFoodList(strCategory)}
            >
                <Image
                    source={
                        image[3]['#text'] ? {uri: image[3]['#text']} : APP_LOGO
                    }
                    style={[
                        styles.trackImageStyle,
                        {
                            width: width / 2 - 30,
                        },
                    ]}
                />
                <View style={styles.vwNameStyle}>
                    <Label style={styles.titleStyle} color={Color.BLACK}>
                        {name}
                    </Label>
                    <Label xsmall style={styles.titleStyle} color={Color.BLACK}>
                        {artist.name}
                    </Label>
                </View>
            </Ripple>
        );
    };

    return (
        // <ImageBackground style={styles.backgroundImage} source={APP_BACKGROUND}>
        <View style={styles.container}>
            <ScrollView style={CommonStyle.master_full_flex}>
                <View style={styles.paddingBottom}>
                    {/* Tag Top Track Data */}

                    <View style={styles.vwTrendingSongs}>
                        <Label
                            mb={10}
                            mt={10}
                            ms={ThemeUtils.relativeWidth(2) + 20}
                            large>
                            {Strings.trendingSongs}
                        </Label>
                        <FlatList
                            style={styles.flatListStyle}
                            data={data}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item) => item.url.toString()}
                            showsHorizontalScrollIndicator={false}
                            horizontal
                        />
                    </View>
                    {/* Top Albums Data */}
                    <View style={styles.vwEvergreenHits}>
                        <Label
                            mb={10}
                            ms={ThemeUtils.relativeWidth(2) + 20}
                            large>
                            {Strings.evergreenHits}
                        </Label>
                        <FlatList
                            style={styles.flatListStyle}
                            data={topAlbumData}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item) => item.url.toString()}
                            showsHorizontalScrollIndicator={false}
                            horizontal
                        />
                    </View>
                    {/* User Top Tracks */}
                    <View style={styles.vwEvergreenHits}>
                        <Label
                            mb={10}
                            ms={ThemeUtils.relativeWidth(2) + 20}
                            large>
                            {Strings.topHits}
                        </Label>
                        <FlatList
                            style={styles.flatListStyle}
                            data={userTopTrackData}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item) => item.url.toString()}
                            showsHorizontalScrollIndicator={false}
                            horizontal
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
        // </ImageBackground>
    );
};

export default NewSongs;
