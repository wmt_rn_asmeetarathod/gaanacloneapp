import React, {useEffect, useState} from 'react';

import {
    View,
    FlatList,
    ImageBackground,
    Image,
    Dimensions,
    ScrollView,
} from 'react-native';
import {APIRequest} from 'src/api';
import {styles} from './styles';

import {Label, Ripple} from 'src/component';
import {CommonStyle, Color, ThemeUtils, Strings} from 'src/utils';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpg';
import APP_LOGO from 'src/assets/images/app_logo.png';

const TopHits = (props) => {
    const [data, setData] = useState([]);
    const width = Dimensions.get('window').width;

    /*  Life-cycles Methods */
    useEffect(() => {
        // Tag Top Tracks
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(
                '/2.0/?method=artist.gettopalbums&artist=cher&api_key=c6dddbc6cda161751a9266b641f0c1b9&format=json',
            )
            .response(onResponse)
            .error(onError)
            .build()
            .doRequest();
    }, []);

    /*  Public Interface Methods */

    /*  Validation Methods  */

    /*  UI Events Methods   */

    // Get Tag Top Tracks
    const onResponse = (res) => {
        console.log('onResponse TopHits res ==>> ', res);
        setData(res.data.topalbums.album);
    };

    const onError = (e) => {
        console.log('onError TopHits e ==>> ', e);
    };

    const onPressIcon = () => {};

    /*  Custom-Component sub-render Methods */
    const RenderItemCompo = ({itemData}) => {
        const {name, artist, image} = itemData;

        return (
            <Ripple
                style={[styles.itemStyle, {width: width / 2 - 30}]}
                rippleContainerBorderRadius={20}
                // onPress={() => goToFoodList(strCategory)}
            >
                <Image
                    source={
                        image[3]['#text'] ? {uri: image[3]['#text']} : APP_LOGO
                    }
                    style={[
                        styles.trackImageStyle,
                        {
                            width: width / 2 - 30,
                        },
                    ]}
                />
                <View style={styles.vwNameStyle}>
                    <Label style={styles.titleStyle} color={Color.BLACK}>
                        {name}
                    </Label>
                    <Label xsmall style={styles.titleStyle} color={Color.BLACK}>
                        {artist.name}
                    </Label>
                </View>
            </Ripple>
        );
    };

    return (
        // <ImageBackground style={styles.backgroundImage} source={APP_BACKGROUND}>
        <View style={styles.container}>
            <ScrollView style={CommonStyle.master_full_flex}>
                <View style={styles.paddingBottom}>
                    {/* Tag Top Track Data */}
                    <View style={styles.vwTrendingSongs}>
                        <Label
                            mb={10}
                            mt={10}
                            ms={ThemeUtils.relativeWidth(2) + 20}
                            large>
                            {Strings.trendingSongs}
                        </Label>
                        <FlatList
                            style={styles.flatListStyle}
                            data={data}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item) => item.url.toString()}
                            showsHorizontalScrollIndicator={false}
                            horizontal
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
        // </ImageBackground>
    );
};

export default TopHits;
