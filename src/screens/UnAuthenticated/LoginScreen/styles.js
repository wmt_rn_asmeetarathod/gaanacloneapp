import {StyleSheet} from 'react-native';
import {ThemeUtils, Color} from 'src/utils';

export default StyleSheet.create({
    container: {
        flex: 1,
        marginTop: ThemeUtils.relativeRealHeight(8),
    },
    mainContainer: {
        flex: 1,
        justifyContent: 'space-around',
        padding: 20,
    },
    vwTitle: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        paddingHorizontal: ThemeUtils.relativeRealWidth(4),
        paddingVertical: ThemeUtils.relativeRealHeight(2),
    },
    centerButton: {
        alignItems: 'center',
    },
    btnStyle: {
        flex: 1,
        alignSelf: 'center',
    },
    logoVw: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 40,
    },
    logoStyle: {
        height: ThemeUtils.relativeWidth(25),
        width: ThemeUtils.relativeWidth(25),
        marginBottom: 10,
        // borderRadius: ThemeUtils.relativeWidth(9),
        // backgroundColor : Color.WHITE,
    },
    forgotPasswordLabel: {
        alignSelf: 'flex-end',
        marginTop: 20,
    },
    signUpLabel: {
        // marginTop: 50,
        width: ThemeUtils.relativeRealWidth(90),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    vwSignUpLabel: {
        alignItems: 'center',
        justifyContent: 'center',
    },
});
