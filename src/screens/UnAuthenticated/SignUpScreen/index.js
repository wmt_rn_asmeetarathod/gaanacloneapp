import React from 'react';

import {
    ImageBackground,
    View,
    Image,
    ScrollView,
    ToastAndroid,
} from 'react-native';
import {AppButton, AppInput, Label, Ripple} from 'src/component';

import {Controller, useForm} from 'react-hook-form';

import {
    CommonStyle,
    Constants,
    Messages,
    ThemeUtils,
    Color,
    Strings,
} from 'src/utils';
import styles from './styles';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpg';
import APP_LOGO from 'src/assets/images/app_logo.png';
import {CommonActions} from '@react-navigation/native';
import Routes from 'src/router/Routes';

const SignUp = (props) => {
    /*  Life-cycles Methods */
    const {
        register,
        setValue,
        control,
        reset,
        clearErrors,
        getValues,
        handleSubmit,
        formState: {errors},
    } = useForm();

    /*  Public Interface Methods */
    /*  Validation Methods  */
    /*  UI Events Methods   */
    /*  Custom-Component sub-render Methods */

    const onSubmit = (data) => {
        console.log('onSubmitted');
        console.log('SignUp data ==>> ', data);
        props.navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    {
                        name: Routes.Authenticated,
                        state: {
                            routes: [
                                {
                                    name: Routes.Category,
                                },
                            ],
                        },
                    },
                ],
            }),
        );
        ToastAndroid.show(Strings.signUpSuccessfully, ToastAndroid.SHORT);
    };

    const onError = (e) => {
        console.log('e ==>> ', e);
    };

    return (
        <ImageBackground style={styles.backgroundImage} source={APP_BACKGROUND}>
            <View style={styles.mainContainer}>
                <View>
                    <View style={styles.logoVw}>
                        <Image style={styles.logoStyle} source={APP_LOGO} />
                    </View>
                    <View style={styles.vwTitle}>
                        <Label xlarge color={Color.WHITE}>
                            {Strings.signUpCaps}
                        </Label>
                    </View>
                    <Controller
                        control={control}
                        render={({onChange, onBlur, value}) => (
                            <AppInput
                                onFocus={() => clearErrors('fullName')}
                                error={errors?.fullName?.message}
                                value={value}
                                label={'Full Name'}
                                baseColor={Color.PINK01}
                                tintColor={Color.PINK01}
                                textColor={Color.WHITE}
                                onBlur={onBlur}
                                onChangeText={onChange}
                            />
                        )}
                        name={'fullName'}
                        defaultValue={''}
                        rules={{
                            required: Messages.Errors.fullNameBlank,
                        }}
                    />
                    <Controller
                        control={control}
                        render={({onChange, onBlur, value}) => (
                            <AppInput
                                onFocus={() => clearErrors('email')}
                                error={errors?.email?.message}
                                value={value}
                                label={'Email'}
                                baseColor={Color.PINK01}
                                tintColor={Color.PINK01}
                                textColor={Color.WHITE}
                                onBlur={onBlur}
                                onChangeText={onChange}
                            />
                        )}
                        name={'email'}
                        defaultValue={''}
                        rules={{
                            required: Messages.Errors.emailBlank,
                            pattern: {
                                value: Constants.Regex.PASSWORD,
                                message: Messages.Errors.emailValidity,
                            },
                        }}
                    />

                    <Controller
                        control={control}
                        render={({onChange, onBlur, value}) => (
                            <AppInput
                                onFocus={() => clearErrors('password')}
                                error={errors?.password?.message}
                                value={value}
                                label={'Password'}
                                secureTextEntry
                                baseColor={Color.PINK01}
                                tintColor={Color.PINK01}
                                textColor={Color.WHITE}
                                onBlur={onBlur}
                                onChangeText={onChange}
                            />
                        )}
                        name={'password'}
                        defaultValue={''}
                        rules={{required: 'Password is required.'}}
                    />
                    <Controller
                        control={control}
                        render={({onChange, onBlur, value}) => (
                            <AppInput
                                onFocus={() => clearErrors('confirmPassword')}
                                error={errors?.confirmPassword?.message}
                                value={value}
                                label={'Confirm Password'}
                                secureTextEntry
                                baseColor={Color.PINK01}
                                tintColor={Color.PINK01}
                                textColor={Color.WHITE}
                                onBlur={onBlur}
                                onChangeText={onChange}
                            />
                        )}
                        name={'confirmPassword'}
                        defaultValue={''}
                        rules={{required: 'Password is required.'}}
                    />
                    <View style={styles.centerButton}>
                        <AppButton
                            click={onSubmit}
                            width={ThemeUtils.relativeRealWidth(82)}
                            mt={ThemeUtils.relativeRealHeight(4)}
                            backgroundColor={Color.PINK01}
                            textColor={Color.WHITE}
                            style={styles.btnStyle}>
                            {Strings.signUpCaps}
                        </AppButton>
                    </View>
                </View>
            </View>
        </ImageBackground>
    );
};

export default SignUp;
