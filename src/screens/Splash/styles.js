import {StyleSheet} from 'react-native';
import {Color} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Color.PRIMARY_BG,
    },
    vwLogo: {
        width: '80%',
        height: '70%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        width: '50%',
        height: '30%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainContainer: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: Color.BLACK,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'contain',
        zIndex: -2,
        width: null,
        height: null,
        opacity: 0.6,
    },
});
